<?php

include 'Singer.php';
class Eurovission
{
    private $singers;

    public function __construct()
    {
        $this->singers = [
            new Singer('Chester', 'UK', 23),
            new Singer('George', 'UA', 41),
            new Singer('Floyd', 'UK', 14),
            new Singer('Mark', 'DB', 14),
        ];
    }

    public function getSingerByCountry($country){
        foreach ($this->singers as $singer)
            if ($singer->getCountry() == $country)
                return $singer->getFio();
        return null;
    }

    public function getSingerByFIO($fio){
        foreach ($this->singers as $singer)
            if ($singer->getFio() == $fio)
                return $singer->getRating();
        return null;
    }
}