<?php

use PHPUnit\Framework\TestCase;

include 'Eurovission.php';

class EV_Test extends TestCase
{
    private $eurovission;

    protected function setUp(): void
    {
        $this->eurovission = new Eurovission();
    }

    public function testSingerFIO(){
        $this->assertEquals('Mark', $this->eurovission->getSingerByCountry('DB'));
    }

    /**
     * @dataProvider addDataProvider
     */
    public function testSingerRating($fio, $expected){

        $this->assertEquals($expected, $this->eurovission->getSingerByFIO($fio));
    }

    public function addDataProvider(){
        return [
            ['George', 41],
            ['Floyd', 14],
            ['Chester', 23],
            ['Mark', 15],
        ];
    }


    protected function tearDown(): void
    {
        unset($this->eurovission);
    }
}