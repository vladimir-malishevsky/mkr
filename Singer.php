<?php


class Singer
{
    private $fio;
    private $country;
    private $rating;

    /**
     * Singer constructor.
     * @param $fio
     * @param $country
     * @param $rating
     */
    public function __construct($fio, $country, $rating)
    {
        $this->fio = $fio;
        $this->country = $country;
        $this->rating = $rating;
    }

    /**
     * @return mixed
     */
    public function getFio()
    {
        return $this->fio;
    }

    /**
     * @param mixed $fio
     */
    public function setFio($fio)
    {
        $this->fio = $fio;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param mixed $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }

}